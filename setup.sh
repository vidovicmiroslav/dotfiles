#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Info:
#   author:    Miroslav Vidovic
#   file:      setup.sh
#   created:   07.02.2017.-14:29:26
#   revision:  ---
#   version:   1.0
# -----------------------------------------------------------------------------
# Requirements: 
# 
# Description:
# 
# Usage:
# 
# -----------------------------------------------------------------------------
# Script:

separator() {
  echo "-------------"
}

check_app() {
  local app="$1"

  if type "$app" 2>/dev/null; then
    echo "$app is installed."
  else
    echo "Error: You should install $app." 
    exit 1
  fi
}

check_dependencies() {
  echo "Checking dependencies..."
  local dependencies=("$@")

  for i in "${dependencies[@]}"; do
    if hash "$i" 2>/dev/null; then
      echo "$i is installed."
    else
      echo "Error: You should install $i" 
    fi
  done
}

setup_ranger() {
  local app="ranger"
  echo "$app"
  separator

  check_app "$app"

  dependencies=(file highlight atool lynx pdftotext exiftool odt2txt)

  if [ ! -d $HOME/.config/ranger ]; then
    mkdir -p $HOME/.config/ranger;
  fi

  check_dependencies "${dependencies[@]}"
  cp -iv ranger/* $HOME/.config/ranger
}

setup_newsbeuter() {
  local app="newsbeuter"
  echo "$app"
  separator

  check_app "$app"

  if [ ! -d $HOME/.newsbeuter ]; then
    mkdir -p $HOME/.newsbeuter;
  fi

  cp -iv newsbeuter/* $HOME/.newsbeuter
}

setup_fish() {
  local app="fish"
  echo "$app"
  separator

  check_app "$app"

  if [ ! -d $HOME/.config/fish ]; then
    mkdir -p $HOME/.config/fish;
  fi

  cp -riv fish/* $HOME/.config/fish
}

setup_conky() {
  local app="conky"
  echo "$app"
  separator

  check_app "$app"

  cp -iv conky/conkyrc $HOME/.conkyrc
}

setup_mpv() {
  local app="mpv"
  echo "$app"
  separator
  
  check_app "$mpv"

  if [ ! -d $HOME/.config/mpv ]; then
    mkdir -p $HOME/.config/mpv;
  fi

  cp -iv mpv/config $HOME/.config/mpv/config
}

main() {
  setup_ranger  
  setup_newsbeuter
  setup_fish
  setup_conky
}

main "$@"

exit 0
