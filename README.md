# dotfiles #

This repository contains various dotfiles (configuration files on a Linux system)

## Currently in the repository  ##

### fish ###
Fish shell configuration, prompt style and functions.

### ranger ###
Ranger file manager settings and key bindings.

### newsbeuter ###
Newsbeuter RSS reader configuration, styles and feeds.

### conky ###
Conky system monitor style and settings.

### mpv ###
mpv video player settings.

* .bash_aliases
* .bashrc
* .gitconfig
* .inputrc
* mpv/config
* .sqliterc
* .tmux.conf
* .vimrc
* .zshrc
